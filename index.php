<?php
require_once __DIR__ . "/autoload.php";

$user = new SuperUser("User User", "User", "pass4", "role");
$user->showInfo();
$user_array = $user->getInfo();
foreach ($user_array as $key_user_array => $value_user_array) {
    echo $value_user_array . "<br>";
}

$user1 = new User("Oleg Moskalenko", "oleg", "pass1");
$user2 = new User("Vasya Pupkin", "vasya", "pass2");
$user3 = new User("Petya Ivanov", "petya", "pass3");

$user1->showInfo();
$user2->showInfo();
$user3->showInfo();

echo "Count object class User " . User::$userCount . "<br>";
echo "Count object class SuperUser " . SuperUser::$superuserCount . "<br>";