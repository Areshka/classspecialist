<?php

/*Пользователь вводит номер дня недели. Выведите список названий всех дней недели в столбец, при этом день, соответствующий данному номеру, выделите большим размером шрифта и другим цветом.*/

$weekDay = array(
    '1' => "Monday",
    '2' => "Tuesday",
    '3' => "Wednesday",
    '4' => "Thursday",
    '5' => "Friday",
    '6' => "Saturday",
    '7' => "Sunday",
);

if ($_SERVER['REQUEST_METHOD'] == "POST") :
    $numberDay = $_POST['number_day'];
?>

<table>
    <?php
        foreach ($weekDay as $weekDayKey => $weekDayValue) :
        if ($weekDayKey == $numberDay) :
            $style = 'style = "color: red; font: italic bold 22px Arial"';
    ?>
            <tr>
                <td <?= $style; ?>>
                    <?= $weekDayValue; ?>
                </td>
            </tr>
            <?php else : ?>
            <tr>
                <td>
                    <?= $weekDayValue; ?>
                </td>
            </tr>
    <?php
        endif;
        endforeach;
    ?>
</table>
<?php
    endif;
?>


<form action="<?= $_SERVER['PHP_SELF']; ?>" method="post">
    <select name="number_day" title="number_day">
        <option <?= $numberDay == 1 ? "selected" : ''; ?> >1</option>
        <option <?= $numberDay == 2 ? "selected" : ''; ?> >2</option>
        <option <?= $numberDay == 3 ? "selected" : ''; ?> >3</option>
        <option <?= $numberDay == 4 ? "selected" : ''; ?> >4</option>
        <option <?= $numberDay == 5 ? "selected" : ''; ?> >5</option>
        <option <?= $numberDay == 6 ? "selected" : ''; ?> >6</option>
        <option <?= $numberDay == 7 ? "selected" : ''; ?> >7</option>
    </select>
    <input type="submit" value="Send">
</form>
