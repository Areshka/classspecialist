<?php

class User extends UserAbstract
{
    public $name;

    public $login;

    public $password;

    public static $userCount;

    public function  __construct($name, $login, $password) {
        $this->name = $name;
        $this->login = $login;
        $this->password = $password;
        ++self::$userCount;
    }

    public function __destruct() {
        echo "User " . $this->login . " removed";
        $this->enter();
    }

    public function showInfo() {
        echo "Name: " . $this->name . "<br>";
        echo "Login: " . $this->login . "<br>";
        echo "Password: " . $this->password . "<br>";
        $this->drawLine();
    }

    public function drawLine() {
        echo "<hr>";
    }

    public function enter() {
        echo "<br>";
    }
}


