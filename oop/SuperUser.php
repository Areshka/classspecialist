<?php

class SuperUser extends User implements SuperUserInterface {

    public $role;

    public static $superuserCount;

    public function __construct($name, $login, $password, $role) {
        parent::__construct($name, $login, $password);
        $this->role = $role;
        ++self::$superuserCount;
    }

    public function showInfo() {
        parent::showInfo();
        echo "Role: " . $this->role . "<br>";
    }

    public function getInfo() {
        $user_array = array(
            "name" => $this->name,
            "login" => $this->login,
            "password" => $this->password,
            "role" => $this->role,
        );
        return $user_array;
    }

}