<?php
define("APP_DIR", __DIR__ . "/oop/");
function __autoload($classname) {
    if (file_exists(APP_DIR . "$classname.php")) {
    require_once (APP_DIR . "$classname.php");
        return true;
    }
    return false;
}